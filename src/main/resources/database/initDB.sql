CREATE TABLE Resource (
    id INT PRIMARY KEY NOT NULL IDENTITY(1,1),
    name VARCHAR(100) NOT NULL
);

CREATE TABLE Action(
    id INT PRIMARY KEY NOT NULL IDENTITY(1,1),
    endpoint varchar(150) NOT NULL,
	label VARCHAR(100) NULL,
	resource_id int NOT NULL,
	authority_id int NULL,
	FOREIGN KEY (resource_id) REFERENCES Resource(id),
	FOREIGN KEY (authority_id) REFERENCES Authority(id)
);

CREATE TABLE Company_Resource (
  company_id int NOT NULL,
  resource_id int NOT NULL,
  CONSTRAINT company_resource_pk PRIMARY KEY (company_id, resource_id),
  FOREIGN KEY (company_id) REFERENCES Company(id),
  FOREIGN KEY (resource_id) REFERENCES Resource(id)
);

-- Add companies
insert into companies ('Business')

-- Add resources
insert into RESOURCE values ('profile-service');

insert into 

-- Add authorities
insert into Authority values ('ROLE_CREATE_RESOURCE');
insert into Authority values ('ROLE_UPDATE_RESOURCE');
insert into Authority values ('ROLE_DELETE_RESOURCE');
insert into Authority values ('ROLE_GET_RESOURCES');
insert into Authority values ('ROLE_GET_RESOURCE_ID');

insert into Authority values ('ROLE_CREATE_ACTION');
insert into Authority values ('ROLE_UPDATE_ACTION');
insert into Authority values ('ROLE_DELETE_ACTION');
insert into Authority values ('ROLE_GET_ACTIONS');
insert into Authority values ('ROLE_GET_ACTION_ID');
insert into Authority values ('ROLE_GET_ACTION_RESOURCENAME');

insert into Authority values ('ROLE_CREATE_AUTHORITY');
insert into Authority values ('ROLE_UPDATE_AUTHORITY');
insert into Authority values ('ROLE_DELETE_AUTHORITY');
insert into Authority values ('ROLE_GET_AUTHORITIES');
insert into Authority values ('ROLE_GET_AUTHORITY_ID');

-- Add Actions
insert into Action (endpoint, label, authority_id, resource_id)
select '/resources/create/**', 'Create resource', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_CREATE_RESOURCE'

insert into Action (endpoint, label, authority_id, resource_id)
select '/resources/update/**', 'Update resource', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_UPDATE_RESOURCE'

insert into Action (endpoint, label, authority_id, resource_id)
select '/resources/delete/**', 'Delete resource', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_DELETE_RESOURCE'

insert into Action (endpoint, label, authority_id, resource_id)
select '/resources', 'Get resources', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_GET_RESOURCES'

insert into Action (endpoint, label, authority_id, resource_id)
select '/resources/id/**', 'Get resource by id', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_GET_RESOURCE_ID'


-- 
insert into Action (endpoint, label, authority_id, resource_id)
select '/actions/create/**', 'Create action', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_CREATE_ACTION'

insert into Action (endpoint, label, authority_id, resource_id)
select '/resources/update/**', 'Update action', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_UPDATE_ACTION'

insert into Action (endpoint, label, authority_id, resource_id)
select '/actions/delete/**', 'Delete action', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_DELETE_ACTION'

insert into Action (endpoint, label, authority_id, resource_id)
select '/actions', 'Get actions', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_GET_ACTIONS'

insert into Action (endpoint, label, authority_id, resource_id)
select '/actions/id/**', 'Get action by id', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_GET_ACTION_ID'

insert into Action (endpoint, label, authority_id, resource_id)
select '/actions/resource/**', 'Get action by endpoint', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_GET_ACTION_RESOURCENAME'


--
insert into Action (endpoint, label, authority_id, resource_id)
select '/authorities/create/**', 'Create authority', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_CREATE_AUTHORITY'

insert into Action (endpoint, label, authority_id, resource_id)
select '/authorities/update/**', 'Update authority', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_UPDATE_AUTHORITY'

insert into Action (endpoint, label, authority_id, resource_id)
select '/authorities/delete/**', 'Delete authority', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_DELETE_AUTHORITY'

insert into Action (endpoint, label, authority_id, resource_id)
select '/authorities', 'Get authorities', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_GET_AUTHORITIES'

insert into Action (endpoint, label, authority_id, resource_id)
select '/authorities/id/**', 'Get authority by id', a.id, (select id from Resource where name = 'profile-service') 
from Authority a where a.authority = 'ROLE_GET_AUTHORITY_ID'