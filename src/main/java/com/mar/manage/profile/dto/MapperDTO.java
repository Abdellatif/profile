package com.mar.manage.profile.dto;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Component;

import com.mar.manage.profile.model.Action;
import com.mar.manage.profile.model.Authority;
import com.mar.manage.profile.model.Resource;
import com.mar.manage.profile.repository.ActionRepository;
import com.mar.manage.profile.repository.AuthorityRepository;
import com.mar.manage.profile.repository.CompanyRepository;
import com.mar.manage.profile.repository.ResourceRepository;

@Component
public final class MapperDTO {
	private ModelMapper modelMapper;
	private ResourceRepository resourceRepository;
	private ActionRepository actionRepository;
	private CompanyRepository companyRepository;
	private AuthorityRepository authorityRepository;
	
	@Autowired
	public MapperDTO(ModelMapper modelMapper,
			ResourceRepository resourceRepository,
			ActionRepository actionRepository,
			CompanyRepository companyRepository,
			AuthorityRepository authorityRepository) {
		this.modelMapper = modelMapper;
		this.resourceRepository = resourceRepository;
		this.actionRepository = actionRepository;
		this.companyRepository = companyRepository;
		this.authorityRepository = authorityRepository;
	}

	/********* Action Mapper ************/
	public ActionDTO convertToDTO(Action entity) {
		ActionDTO actionDTO = this.modelMapper.map(entity, ActionDTO.class);
		
		// Set id resource
		actionDTO.setIdResource( 
				!Objects.isNull(entity.getResource()) ? entity.getResource().getId() : 0);
		
		// Set id authority
		actionDTO.setIdAuthority(
				!Objects.isNull(entity.getAuthority()) ? entity.getAuthority().getId() : 0);
		
		return actionDTO;
	}
	
	public Action convertToEntity(ActionDTO dto) throws ParseException {
		Action action = modelMapper.map(dto, Action.class);
	    
	    // Set Resource
	 	Optional<Resource> resource = resourceRepository.findById(dto.getIdResource());
	 	action.setResource( resource.isPresent() ? resource.get() : null );
	 	
	 	// Set Authority
	 	Optional<Authority> authority = authorityRepository.findById(dto.getIdAuthority());
	 	action.setAuthority( authority.isPresent() ? authority.get() : null );
	 	
	    return action;
	}
	
	/********* authority Mapper ************/
	public AuthorityDTO convertToDTO(Authority entity) {
		AuthorityDTO authoritytDTO = this.modelMapper.map(entity, AuthorityDTO.class);
		
		// Set id action
		authoritytDTO.setIdAction( 
						!Objects.isNull(entity.getAction()) ? entity.getAction().getId() : 0);
				
		return authoritytDTO;
	}
	
	public Authority convertToEntity(AuthorityDTO dto) throws ParseException {
		Authority authority = modelMapper.map(dto, Authority.class);
	    
		// Set Action
		Optional<Action> action= actionRepository.findById(dto.getIdAction());
		authority.setAction( action.isPresent() ? action.get() : null );
	 	
	    return authority;
	}
	
	/********* Resource Mapper ************/
	public ResourceDTO convertToDTO(Resource entity) {
		ResourceDTO resourceDTO = this.modelMapper.map(entity, ResourceDTO.class);
		
		// Set id companies
		if(!Objects.isNull(entity.getCompanies())) {
			resourceDTO.setIdCompanies( 
					entity.getCompanies().stream()
						.map( company -> company.getId())
						.collect(Collectors.toSet())
			);
		}
		
		return resourceDTO;
	}
	
	public Resource convertToEntity(ResourceDTO dto) throws ParseException {
		Resource resource = modelMapper.map(dto, Resource.class);
	    
	    // Set Actions
		if(!Objects.isNull(dto.getIdCompanies())) {
			resource.setCompanies( 
					dto.getIdCompanies().stream()
						.filter(id -> companyRepository.findById(id).isPresent())
						.map( id -> companyRepository.findById(id).get())
						.collect(Collectors.toSet())
			);
		}
	 	
	    return resource;
	}
}
