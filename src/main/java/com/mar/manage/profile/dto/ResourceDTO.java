package com.mar.manage.profile.dto;

import java.util.Set;

public class ResourceDTO {
	private long id;
	private String name;
	private Set<Long> idCompanies;
	
	/************* GETTERS AND SETTERS ******/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<Long> getIdCompanies() {
		return idCompanies;
	}
	public void setIdCompanies(Set<Long> idCompanies) {
		this.idCompanies = idCompanies;
	}
}
