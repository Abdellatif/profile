package com.mar.manage.profile.dto;

public class ActionDTO {
	private long id;
	private String endpoint;
	private String label;
	private long idResource;
	private long idAuthority;
	
	/************* GETTERS AND SETTERS ******/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public long getIdResource() {
		return idResource;
	}
	public void setIdResource(long idResource) {
		this.idResource = idResource;
	}
	public long getIdAuthority() {
		return idAuthority;
	}
	public void setIdAuthority(long idAuthority) {
		this.idAuthority = idAuthority;
	}
}
