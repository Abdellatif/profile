package com.mar.manage.profile.dto;

public class AuthorityDTO {
	private long id;
	private String authority;
	private long idAction;
	
	/************* GETTERS AND SETTERS ******/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public long getIdAction() {
		return idAction;
	}
	public void setIdAction(long idAction) {
		this.idAction = idAction;
	}
}
