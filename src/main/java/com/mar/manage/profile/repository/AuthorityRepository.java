package com.mar.manage.profile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mar.manage.profile.model.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
