package com.mar.manage.profile.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mar.manage.profile.model.Resource;

public interface ResourceRepository extends JpaRepository<Resource, Long> {
	public List<Resource> findAll();
	public Optional<Resource> findByName(String name);
}
