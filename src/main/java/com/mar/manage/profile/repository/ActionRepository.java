package com.mar.manage.profile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mar.manage.profile.model.Action;
import com.mar.manage.profile.model.Resource;

public interface ActionRepository extends JpaRepository<Action, Long>{
	List<Action> findByResource(Resource resource);
}
