package com.mar.manage.profile.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mar.manage.profile.model.Company;

public interface CompanyRepository extends JpaRepository<Company, Long>{
}
