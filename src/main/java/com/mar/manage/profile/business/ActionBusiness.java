package com.mar.manage.profile.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;
import javax.validation.Valid;

import com.mar.manage.profile.exception.ModelNotValideException;
import com.mar.manage.profile.dto.ActionDTO;
import com.mar.manage.profile.dto.MapperDTO;
import com.mar.manage.profile.exception.ActionNotFoundException;
import com.mar.manage.profile.model.Action;
import com.mar.manage.profile.repository.ResourceRepository;
import com.mar.manage.profile.repository.ActionRepository;

@Component
public class ActionBusiness {
	
	protected ActionRepository actionRepository;
	protected ResourceRepository resourceRepository;
	protected MapperDTO mapperDTO;
	Validator validator;
	
	@Autowired
	public ActionBusiness(MapperDTO mapperDTO,
			ActionRepository actionRepository,
			ResourceRepository resourceRepository,
			Validator validator) {
		this.mapperDTO = mapperDTO;
		this.actionRepository = actionRepository;
		this.resourceRepository = resourceRepository;
		this.validator = validator;
	}
	
	public Action buildNewAction(ActionDTO actionDTO) 
			throws ModelNotValideException {
		
		// Build Action from actionDTO
		Action action= mapperDTO.convertToEntity(actionDTO);
		
		// Set id
		action.setId(0);
		
		// Validate the action
		if(!isValide(action)) {
			throw new ModelNotValideException();
		}
		
		return action;
	}
	
	public Action buildExistedAction(ActionDTO actionDTO) 
			throws ActionNotFoundException, ModelNotValideException {
		
		// Check the existence of the object
		if( !actionRepository.existsById(actionDTO.getId()) ) {
			throw new ActionNotFoundException(Long.toString(actionDTO.getId()));
		}
		
		// Build action from actionDTO
		Action action = mapperDTO.convertToEntity(actionDTO);
		
		// Validate the product
		if(!isValide(action)) {
			throw new ModelNotValideException();
		}
		
		return action;
	}
	
	// Validate the action data
	public boolean isValide(@Valid Action action) {
		return true;
	}
}
