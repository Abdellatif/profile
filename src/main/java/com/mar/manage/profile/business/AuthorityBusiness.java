package com.mar.manage.profile.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mar.manage.profile.dto.MapperDTO;
import com.mar.manage.profile.dto.AuthorityDTO;
import com.mar.manage.profile.exception.AuthorityNotFoundException;
import com.mar.manage.profile.exception.ModelNotValideException;
import com.mar.manage.profile.model.Authority;
import com.mar.manage.profile.repository.AuthorityRepository;

@Component
public class AuthorityBusiness {
	
	protected AuthorityRepository authorityRepository;
	protected MapperDTO mapperDTO;
	
	@Autowired
	public AuthorityBusiness(
			AuthorityRepository authorityRepository,
			MapperDTO mapperDTO) {
		this.authorityRepository = authorityRepository;
		this.mapperDTO = mapperDTO;
	}
	
	public Authority buildNewAuthority(AuthorityDTO authorityDTO) 
			throws ModelNotValideException {
		
		// Build authority from authorityDTO
		Authority authority = mapperDTO.convertToEntity(authorityDTO);
		
		// Set id, createDate and lastUpdatedDate
		authority.setId(0);
		//authority.setCreateDate(new Date());
		//authority.setLastUpdateDate(null);
		
		// Validate the authority
		if(!isValide()) {
			throw new ModelNotValideException();
		}
		
		return authority;
	}
	
	public Authority buildExistedAuthority(AuthorityDTO authorityDTO) 
			throws AuthorityNotFoundException, ModelNotValideException {
		
		// Check the existence of the object
		if( !authorityRepository.existsById(authorityDTO.getId()) ) {
			throw new AuthorityNotFoundException(Long.toString(authorityDTO.getId()));
		}
		
		// Build product from productDTO
		Authority authority = mapperDTO.convertToEntity(authorityDTO);
		
		// Set createDate and lastUpdateDate
		//authority.setLastUpdateDate(new Date());
				
		// Validate the product
		if(!isValide()) {
			throw new ModelNotValideException();
		}
		
		return authority;
	}
	
	// Validate the authority data
	public boolean isValide() {
		return true;
	}
}
