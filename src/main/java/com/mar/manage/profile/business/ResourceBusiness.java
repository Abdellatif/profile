package com.mar.manage.profile.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mar.manage.profile.exception.ResourceNotFoundException;
import com.mar.manage.profile.exception.ModelNotValideException;
import com.mar.manage.profile.model.Resource;
import com.mar.manage.profile.repository.ResourceRepository;

@Component
public class ResourceBusiness {
	
	protected ResourceRepository resourceRepository;
	
	@Autowired
	public ResourceBusiness(ResourceRepository resourceRepository) {
		this.resourceRepository = resourceRepository;
	}
	
	// Create new resource
	public Resource buildNewResource(Resource resource) 
			throws ModelNotValideException {
		
		// Set id
		resource.setId(0);
		
		// Validate the resource
		if(!isValide()) {
			throw new ModelNotValideException();
		}
		
		return resource;
	}
	
	// Create existed resource
	public Resource buildExistedResource(Resource resource) 
			throws ResourceNotFoundException, ModelNotValideException {
		
		// Check the existence of the object
		if( !resourceRepository.existsById(resource.getId()) ) {
			throw new ResourceNotFoundException(Long.toString(resource.getId()));
		}
		
		// Validate the product
		if(!isValide()) {
			throw new ModelNotValideException();
		}
		
		return resource;
	}
	
	// Validate the resource data
	public boolean isValide() {
		return true;
	}
}
