package com.mar.manage.profile.exception;


import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ModelNotValideException extends RuntimeException  {
	private static final long serialVersionUID = 1L;
	
	public ModelNotValideException() {
		super("Model not valide");
	}
}
