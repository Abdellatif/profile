package com.mar.manage.profile.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AuthorityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AuthorityNotFoundException(String criteria) {
		super("No such Authority : " + criteria);
	}
}
