package com.mar.manage.profile.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mar.manage.profile.business.ResourceBusiness;
import com.mar.manage.profile.exception.ResourceNotFoundException;
import com.mar.manage.profile.exception.ModelNotValideException;
import com.mar.manage.profile.model.Resource;
import com.mar.manage.profile.repository.ResourceRepository;

@RestController
@RequestMapping("/resources")
public class RousourceRestController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RousourceRestController.class);
	protected ResourceRepository resourceRepository;
	protected ResourceBusiness resourceBusiness;
	
	@Autowired
	public RousourceRestController(ResourceRepository resourceRepository,
			ResourceBusiness resourceBusiness) {
		
		LOGGER.info("ResourceRestController cosntructor has been called");
		this.resourceRepository = resourceRepository;
		this.resourceBusiness = resourceBusiness; 
	}

	/**
	 * @param id
	 * @return resource
	 */
	// Get entity by id
	@GetMapping("id/{id}")
	public ResponseEntity<Resource> getEntityById(@PathVariable("id") long id) 
			throws ResourceNotFoundException {
		
		LOGGER.info("resource-service byId() " + id);

		Optional<Resource> resource = resourceRepository.findById(id);
		
		if( !resource.isPresent() ) {
			throw new ResourceNotFoundException(Long.toString(id));
		}
		
		return new ResponseEntity<>(
				resource.get(),
				HttpStatus.OK);
	}
	
	/**
	 * @param
	 * @return all resources
	 */
	// Get entities
	@GetMapping("/")
	public ResponseEntity<List<Resource>> getAllEntities() {
		List<Resource> result = resourceRepository.findAll();
		
		return new ResponseEntity<>(
				result,
				HttpStatus.OK);
	}
	
	/**
	 * @param resource
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	// Create entity
	@PostMapping("create")
	public ResponseEntity<Resource> saveEntity(@Valid @RequestBody Resource resource ) 
			throws MethodArgumentNotValidException, ModelNotValideException  {
		
		LOGGER.info("Resource-service Save");
		
		// generate new resource 
		resource = resourceBusiness.buildNewResource(resource);
				
		resourceRepository.save(resource);
		
		return new ResponseEntity<>(
				resource,
				HttpStatus.OK);
	}
	
	/**
	 * @param resource
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	// Update entity
	@PutMapping("update")
	public ResponseEntity<Resource> updateEntity(@Valid @RequestBody Resource resource ) 
			throws ResourceNotFoundException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("resource-service Save");
		
		// generate existed resource 
		resource = resourceBusiness.buildExistedResource(resource);

		resourceRepository.save(resource);
		
		return new ResponseEntity<>(
				resource,
				HttpStatus.OK);
	}
	
	// Delete entity
	@DeleteMapping("delete/{id}")
	public ResponseEntity<Long> deleteEntity(@PathVariable long id) throws ResourceNotFoundException {
		
		if ( !resourceRepository.existsById(id) ) {
			throw new ResourceNotFoundException((new Long(id)).toString());
		}
		
		resourceRepository.deleteById(id);
		return new ResponseEntity<>(
				new Long(id),
				HttpStatus.OK);
	}
}
