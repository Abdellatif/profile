package com.mar.manage.profile.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mar.manage.profile.business.AuthorityBusiness;
import com.mar.manage.profile.dto.AuthorityDTO;
import com.mar.manage.profile.dto.MapperDTO;
import com.mar.manage.profile.exception.ModelNotValideException;
import com.mar.manage.profile.exception.AuthorityNotFoundException;
import com.mar.manage.profile.model.Authority;
import com.mar.manage.profile.repository.AuthorityRepository;


@RestController
@RequestMapping("/authorities")
public class AuthorityRestController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityRestController.class);
	protected AuthorityRepository authorityRepository;
	protected AuthorityBusiness authorityBusiness;
	protected MapperDTO mapperDTO;

	@Autowired
	public AuthorityRestController(AuthorityRepository authorityRepository,
			AuthorityBusiness authorityBusiness,
			MapperDTO mapperDTO) {
		
		LOGGER.info("AuthorityRestController cosntructor has been called");
		this.authorityRepository = authorityRepository;
		this.authorityBusiness = authorityBusiness;
		this.mapperDTO = mapperDTO;
	}

	/**
	 * @param id
	 * @return Authority
	 */
	// Get entity by id
	@GetMapping("id//{id}")
	public ResponseEntity<AuthorityDTO> getAuthorityById(@PathVariable("id") long id) 
			throws AuthorityNotFoundException {
		
		LOGGER.info("Authorities-service byId() " + id);

		Optional<Authority> authority = authorityRepository.findById(id);
		
		if( !authority.isPresent() ) {
			throw new AuthorityNotFoundException(Long.toString(id));
		}
		
		return new ResponseEntity<>(
				this.mapperDTO.convertToDTO(authority.get()),
				HttpStatus.OK);
	}
	
	/**
	 * @param
	 * @return all authorities
	 */
	// Get all entities
	@GetMapping("/")
	public ResponseEntity<List<AuthorityDTO>> getAllEntities() {
		List<Authority> result = authorityRepository.findAll();
		
		List<AuthorityDTO> resultDTO = result.stream()
				.map(item -> mapperDTO.convertToDTO(item))
				.collect(Collectors.toList());
		
		return new ResponseEntity<>(
				resultDTO,
				HttpStatus.OK);
	}
	
	/**
	 * @param authority
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	
	// Create entity
	@PostMapping("create")
	public ResponseEntity<AuthorityDTO> saveEntity(@Valid @RequestBody AuthorityDTO authorityDTO) 
			throws MethodArgumentNotValidException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("Authorities-service Save");
		
		// Generate new authority
		Authority authority = this.authorityBusiness.buildNewAuthority(authorityDTO);
		
		authorityRepository.save(authority);
		
		return new ResponseEntity<>(
				authorityDTO,
				HttpStatus.OK);
	}
	
	/**
	 * @param authority
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	
	// Update entity
	@PutMapping("update")
	public ResponseEntity<AuthorityDTO> updateEntity(@Valid @RequestBody AuthorityDTO authorityDTO) 
			throws MethodArgumentNotValidException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("Authorities-service update");
		
		// Generate new authority
		Authority authority = this.authorityBusiness.buildExistedAuthority(authorityDTO);
		
		authorityRepository.save(authority);
		
		return new ResponseEntity<>(
				authorityDTO,
				HttpStatus.OK);
	}
	
	// Delete entity
	@DeleteMapping("delete/{id}")
	public ResponseEntity<Long> deleteEntity(@PathVariable long id) throws AuthorityNotFoundException {
		
		if ( !authorityRepository.existsById(id) ) {
			throw new AuthorityNotFoundException((new Long(id)).toString());
		}
		
		authorityRepository.deleteById(id);
		return new ResponseEntity<>(
				new Long(id),
				HttpStatus.OK);
	}
}
