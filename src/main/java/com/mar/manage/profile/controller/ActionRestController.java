package com.mar.manage.profile.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mar.manage.profile.dto.ActionDTO;
import com.mar.manage.profile.dto.MapperDTO;
import com.mar.manage.profile.business.ActionBusiness;
import com.mar.manage.profile.exception.ModelNotValideException;
import com.mar.manage.profile.exception.ResourceNotFoundException;
import com.mar.manage.profile.exception.ActionNotFoundException;
import com.mar.manage.profile.model.Action;
import com.mar.manage.profile.model.Resource;
import com.mar.manage.profile.repository.ActionRepository;
import com.mar.manage.profile.repository.ResourceRepository;


@RestController
@RequestMapping("/actions")
public class ActionRestController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActionRestController.class);
	protected ActionRepository actionRepository;
	protected ResourceRepository resourceRepository;
	protected MapperDTO mapperDTO;
	protected ActionBusiness actionBusiness;

	@Autowired
	public ActionRestController(ActionRepository actionRepository,
			ResourceRepository resourceRepository,
			MapperDTO mapperDTO,
			ActionBusiness actionBusiness) {
		
		LOGGER.info("ActionRestController cosntructor has been called");
		this.actionRepository = actionRepository;
		this.resourceRepository = resourceRepository;
		this.mapperDTO = mapperDTO;
		this.actionBusiness = actionBusiness;
	}

		/**
	 * @param id
	 * @return action
	 */
	// Get entity by id
	@GetMapping("/id/{id}")
	public ResponseEntity<ActionDTO> getEntityById(@PathVariable("id") long id) 
			throws ActionNotFoundException {
		
		LOGGER.info("Action-service byId() " + id);

		Optional<Action> action= actionRepository.findById(id);
		
		if( !action.isPresent() ) {
			throw new ActionNotFoundException(Long.toString(id));
		}
		
		return new ResponseEntity<>(
				this.mapperDTO.convertToDTO(action.get()),
				HttpStatus.OK);
	}
	
	/**
	 * @param
	 * @return all actions
	 */
	// Get all entities
	@GetMapping("/")
	public ResponseEntity<List<ActionDTO>> getAllEntities() {
		List<Action> result = actionRepository.findAll();
		
		List<ActionDTO> resultDTO = result.stream()
				.map(item -> mapperDTO.convertToDTO(item))
				.collect(Collectors.toList());
		
		return new ResponseEntity<>(
				resultDTO,
				HttpStatus.OK);
	}
	
	/**
	 * @param action
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	
	// Create entity
	@PostMapping("create")
	public ResponseEntity<ActionDTO> saveEntity(@Valid @RequestBody ActionDTO actionDTO) 
			throws MethodArgumentNotValidException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("Action-service Save");
		
		// generate new action
		Action action = this.actionBusiness.buildNewAction(actionDTO);
		
		actionRepository.save(action);
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(action),
				HttpStatus.OK);
	}
	
	/**
	 * @param action
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	
	// Update entity
	@PutMapping("update")
	public ResponseEntity<ActionDTO> updateEntity(@Valid @RequestBody ActionDTO actionDTO) 
			throws MethodArgumentNotValidException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("action-service update");
		
		// generate existed action
		Action action = this.actionBusiness.buildExistedAction(actionDTO);
		
		actionRepository.save(action);
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(action),
				HttpStatus.OK);
	}
	
	// Delete entity
	@DeleteMapping("delete/{id}")
	public ResponseEntity<Long> deleteEntity(@PathVariable long id) throws ActionNotFoundException {
		
		if ( !actionRepository.existsById(id) ) {
			throw new ActionNotFoundException((new Long(id)).toString());
		}
		
		actionRepository.deleteById(id);
		return new ResponseEntity<>(
				new Long(id),
				HttpStatus.OK);
	}
	
	// Get Actions by Resource
	@GetMapping("/resource/{name}")
	public List<Action> getEntitiesByResourceName(@PathVariable String name) {
		Optional<Resource> resource= resourceRepository.findByName(name);
		
		if( !resource.isPresent() ) {
			throw new ResourceNotFoundException(name);
		}
		
		return actionRepository.findByResource(resource.get());
	}
}
